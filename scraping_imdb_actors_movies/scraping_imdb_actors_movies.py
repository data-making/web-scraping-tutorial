from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import time
import os


def get_actor_page_soup(actor_url):
    driver = webdriver.Chrome()
    driver.get(actor_url)

    see_all_buttons = driver.find_elements(By.CLASS_NAME, "ipc-see-more__button")

    for see_all_button in see_all_buttons:
        try:        
            driver.execute_script("arguments[0].click();", see_all_button)
            time.sleep(5)
        except Exception as ex:
            print(ex)

    actor_page = driver.page_source
    actor_page_soup = BeautifulSoup(actor_page, "html.parser")
    driver.quit()

    return actor_page_soup


def scrap_actor_movie_data(actor_page_soup):
    actor_name = actor_page_soup.find(class_ = "sc-afe43def-1 fDTGTb").string

    actor_containers = None
    actor_hgroup_h3s = actor_page_soup.select("hgroup h3")
    for hgroup_h in actor_hgroup_h3s:
        if hgroup_h.string == "Actor":
            actor_containers = hgroup_h.parent.parent.next_sibling.children
            actor_containers = list(actor_containers)
            break
    actor_previous_movies_container = actor_containers[-1]
    actor_previous_movies_container = list(actor_previous_movies_container.children)[0]
    actor_previous_movies_container = list(actor_previous_movies_container.children)[-1]
    actor_previous_movies_container = list(actor_previous_movies_container.children)[0]
    actor_previous_movies_container = list(actor_previous_movies_container.children)[0]
    actor_previous_movies_container_li = list(actor_previous_movies_container.children)

    file_path = os.path.dirname(__file__)
    file_name = actor_name.lower().replace(" ", "_") + "_movies.csv"
    file_full_name = os.path.join(file_path, file_name)
    with open(file_full_name, "w") as file:
        file.write("release_year,name,rating,genre,director\n")
        
        no_movies = 0
        for movie_item in actor_previous_movies_container_li:
            # if (no_movies == 1):
            #     break
            movie_item_summary = list(movie_item.children)[1]
            movie_item_summary = list(movie_item_summary.children)
            movie_name_cont = movie_item_summary[0]
            movie_year_cont = movie_item_summary[1]
                    
            movie_year = movie_year_cont.find("span").string
            file.write(movie_year + ",")
            
            movie_anchor = movie_name_cont.find("a")
            movie_name = movie_anchor.string
            movie_name = movie_name.replace(",", "")
            file.write(movie_name + ",")
            
            driver = webdriver.Chrome()
            movie_page_url = "https://imdb.com" + movie_anchor.get("href")
            driver.get(movie_page_url)
            movie_page = driver.page_source
            movie_page_soup = BeautifulSoup(movie_page, "html.parser")
            driver.quit()
            
            movie_imdb_rating_conts = movie_page_soup.find_all(class_ = "rating-bar__base-button")
            movie_imdb_rating_cont = ""
            for container in movie_imdb_rating_conts:
                if (list(container.children)[0]).string == "IMDb RATING":
                    movie_imdb_rating_cont = container
                    break
            movie_imdb_rating = (movie_imdb_rating_cont.find_all(class_ = "iZlgcd")[0]).string
            
            file.write(movie_imdb_rating + ",")

            genre_container = movie_page_soup.find(class_ = "sc-acac9414-4")
            genre_container = genre_container.find(class_ = "ipc-chip-list__scroller")
            genre_container = list(genre_container.children)
            len_genre_container = len(genre_container)
            for index_genre in range(len_genre_container):
                item = genre_container[index_genre]
                genre = item.find("span").string
                if index_genre == len_genre_container-1:
                    genre += ","
                else:
                    genre += "|"
                file.write(genre)

            director_container = movie_page_soup.find(class_ = "ipc-metadata-list ipc-metadata-list--dividers-all sc-bfec09a1-8 iiDmgX ipc-metadata-list--base")
            director_container = list(director_container.children)[0]
            director_container = director_container.find("a")
            director = director_container.string
            file.write(director + "\n")

            no_movies += 1


def start_app():
    print("****************** Program Started *****************")

    actor_url = input("Enter the actor imdb url: ")
    # actor_url = "https://www.imdb.com/name/nm0897201/?ref_=nv_sr_srsg_9_tt_1_nm_7_q_vijay"
    actor_page_soup = get_actor_page_soup(actor_url)
    scrap_actor_movie_data(actor_page_soup)

    print("****************** Program Ended *****************")


start_app()